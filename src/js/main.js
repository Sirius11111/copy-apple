/*  Modernizr - WEPB */
// !find its on libs/modernizr-custom
// Modernizr.on( 'webp', function( result ) {
//   const section = document.querySelectorAll('  ')
//   section.forEach((item) => {
//     if (result) {
//         item.classList.add('webp')
//       } else {
//         item.classList.add('no-webp')
//       }
//     });
//   });
/*  ---------------- */

/*
! 
!      ____                      _              _       
!     / / /   ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
!    / / /   / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
!   / / /   | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
!  /_/_/     \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
!                                                       
! 
*/
let heightHeader;
let count = 0;
const windowWidth = $(window).width();
let items = [];
let isHaveItemsSlayder = false;

$(document).ready(function () {
  $("body").animate({ opacity: 1 });
  // *header
  heightHeader = $("#header").height() / 2;
  // *menu opened
  $("#menu-btn-handler").on("click", function (e) {
    e.preventDefault();
    $("header").toggleClass("open");
    $(this).toggleClass("open").css({
      pointerEvents: "none",
    });
    $("#mobile-menu").slideToggle();
    setTimeout(() => {
      $(this).css({
        pointerEvents: "inherit",
      });
    }, 500);
  });

  if (window.location.pathname === "/") {
    // carousel
    if ($(window).width() < 768) {
      Array.from($("#slayder").children()).forEach((element) => {
        $(element).removeClass((i, c) => {
          let b = c
            .split(" ")
            .filter(
              (a) => a.split("-")[0] === "col" || a.split("-")[0] === "order"
            );
          return b.join(" ");
        });
      });
      $(".best__container").removeClass("container").addClass("wrap");
      $("#slayder")
        .removeClass("row")
        .owlCarousel({
          items: 1,
          margin: 15,
          dots: false,
          nav: true,
          navText: [
            "<img src='./images/svg/arrow-left.svg'>",
            "<img src='./images/svg/arrow-right.svg'>",
          ],
        });
    }
  } else if (window.location.pathname === "/courses.php") {
    //  * ||не трогать все четко||  slick
    $(".favorite-slider").slick({
      centerMode: true,
      centerPadding: "10px",
      speed: 600,
      initialSlide: 2,
      variableWidth: true,
      adaptiveHeight: true,
      arrows: false,
      dots: true,
    });
    // *||не трогать все четко|| ===============================================

    // *popup------------------------------------------
    let active = false;
    $(".popup").on("click", function (e) {
      e.preventDefault();
      // ?! рендер
      const id = $(this).parents(".courses__item-js").attr("data-id");
      if (!isHaveItemsSlayder) {
        for (let i = 0; i < items.length; i++) {
          const element = items[i];
          if (element.id === id) {
            for (let b = i + 1; b < items.length; b++) {
              renderSlayderItem(items[b], "prepend");
            }
            renderSlayderItem(element, "this");
            for (let c = i - 1; c >= 0; c--) {
              renderSlayderItem(items[c], "end");
            }
            break;
          }
        }
        $("#popup-slayder").owlCarousel({
          items: 1,
          margin: 15,
          loop: false,
          stagePadding: 0,
          smartSpeed: 200,
          dragEndSpeed: 400,
          nav: false,
          dots: false,
          video: true,
          autoHeight: true,
          startPosition: "http://alina/courses.php#this",
        });
        $("#popup-slayder").trigger("refresh.owl.carousel");
      }
      // ?! рендер-------------------------------------------

      $("#popup")
        .fadeIn()
        .css({ top: +window.scrollY });
      active = true;
      isHaveItemsSlayder = true;

      // todo *********************************
      // todo Сдлеать выбор элемента по дата id
      // todo *********************************
    });
    // *||не трогать все четко||-----------------------
    $("#close-popup").on("click", function (e) {
      e.preventDefault();
      $("#popup").fadeOut();
      active = false;
    });

    //* закрытие по области
    $(document).on("click", function (e) {
      if ($(e.target).hasClass("po-pup") && active) {
        $("#popup").fadeOut();
        active = false;
      }
    });

    //*||не трогать все четко||=======================

    let list = Array.from($(".courses__item-js"));
    $("#popup-slayder").trigger("destroy.owl.carousel");
    showCourses(list);

    //* ||не трогать все четко|| more-courses-----------------------------------
    $("#more-courses").on("click", function (e) {
      e.preventDefault();
      const newCount = length;
      const listItems = Array.from($(".courses__item-js"));
      if (count === listItems.length) {
        alert("Это были все видео");
      } else {
        for (let i = count; i < listItems.length; i++) {
          if (count !== newCount + 20) {
            const element = $(listItems[i]);
            $(element).show().addClass("animated bounceIn");
            count++;
          }
        }
        if (count === listItems.length) {
          $(".playlist__more-arrow").hide();
        }
      }
    });
    // *-----------------------------------------------

    //* filter-----------------------------------------
    $("input[data-filter]:checkbox").on("change", function (e) {
      e.preventDefault();
      const checkboxes = $("input[data-filter]:checkbox:checked");
      const data = {
        spiker: [],
        university: [],
        trend: [],
        interest: "",
        date: "",
      };
      $(checkboxes).each(function () {
        switch ($(this).attr("data-category")) {
          case "spiker":
            data.spiker.push($(this).val().trim());
            break;
          case "university":
            data.university.push($(this).val().trim());
            break;
          case "trend":
            data.trend.push($(this).val().trim());
            break;
          case "date":
            data.date = 'true';
            break;
          case "interest":
            data.interest = 'true';
            break;
          default:
            break;
        }
      });

      $(".courses__item-js").addClass("bounceOut").removeClass("animated").hide();
      sorting(data);
    });
      //? клик по ссылкам 
    $('.filter-link-js').on('click', function (e) {
      e.preventDefault()
      const id = $(this).attr('data-filter')
      $(`#${id}`).attr('checked',true)
    })

    //* ===============================================
    // *-||не трогать все четко|| slayder-------------------------------------------
    $("#slayder-left-btn").click(function () {
      $("#popup-slayder").trigger("prev.owl.carousel");
    });

    $("#slayder-right-btn").click(function () {
      $("#popup-slayder").trigger("next.owl.carousel");
    });
    // *||не трогать все четко|| ---------------------------------------------------
  } else if (window.location.pathname === "/cooperation.php") {
    console.log("cooperation");
  } else if (window.location.pathname === "/login.php") {
    (function ($) {
      "use strict";
      /*==================================================================
      [ Validate ]*/
      var input = $(".validate-input .input100");

      $(".validate-form").on("submit", function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
          if (validate(input[i]) == false) {
            showValidate(input[i]);
            check = false;
          }
        }

        return check;
      });

      $(".validate-form .input100").each(function () {
        $(this).focus(function () {
          hideValidate(this);
        });
      });

      function validate(input) {
        if (
          $(input).attr("type") == "email" ||
          $(input).attr("name") == "email"
        ) {
          if (
            $(input)
              .val()
              .trim()
              .match(
                /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/
              ) == null
          ) {
            return false;
          }
        } else {
          if ($(input).val().trim() == "") {
            return false;
          }
        }
      }

      function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass("alert-validate");
      }

      function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass("alert-validate");
      }
    })(jQuery);
  } else if (window.location.pathname === "/admin.php") {
    $(".delete").on("click", function () {
      event.preventDefault();
      const id = $(this).parents("tr").attr("data-id");
      $.ajax({
        type: "POST",
        url: "admin.php",
        data: `id=${id}`,
        success: function (response) {
          alert("Успешно удалено");
          $(this).parent("tr").slideDown();
        },
      });
    });
  }
});

// * ||не трогать все четко|| Функция для показа новых курсов
function showCourses(list) {
  isHaveItemsSlayder = false;
  //*появление элементов--------------------------
  $(".playlist__more-arrow").show();
  if (list.length < 9) {
    count = list.length;
    $(".playlist__more-arrow").hide();
  } else {
    count = 9;
  }
  if (windowWidth <= 992) {
    if (list.length < 6) {
      count = list.length;
      $(".playlist__more-arrow").hide();
    } else {
      count = 6;
    }
  } else if (windowWidth <= 480) {
    if (list.length < 3) {
      count = list.length;
      $(".playlist__more-arrow").hide();
    } else {
      count = 3;
    }
  }
  for (let y = 0; y < count; y++) {
    const element = list[y];
    $(element).show().addClass("animated bounceIn");
  }

  //* ===============================================
}
//*=======================================

/*
 *
 *      ____                     _ _    __                  _   _
 *     / / /  ___  ___ _ __ ___ | | |  / _|_   _ _ __   ___| |_(_) ___  _ __
 *    / / /  / __|/ __| '__/ _ \| | | | |_| | | | '_ \ / __| __| |/ _ \| '_ \
 *   / / /   \__ \ (__| | | (_) | | | |  _| |_| | | | | (__| |_| | (_) | | | |
 *  /_/_/    |___/\___|_|  \___/|_|_| |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|
 *
 *
 */
$(window).scroll(function () {
  const scroll = $(window).scrollTop();
  if (scroll >= heightHeader) {
    $("#header").addClass("scrolled");
  } else {
    $("#header").removeClass("scrolled");
  }
});

// *server=================================
function getAllItem() {
  $.ajax({
    url: "ссылка",
    success: function (data) {
      return data;
    },
  });
}
// *=================================

//*popupSlayder---------------------------------
function renderSlayderItem(
  { id, title, description, author, date, link },
  position = null
) {
  const item = `<div class="po-pup__content slayder-item" data-slayder-id="${id}" id=${
    position === "this" ? "this" : ""
  }>
      <div class="po-pup__content__video">
        <iframe src="//cdn.plrjs.com/player/94sd9y38shz29/pkqtn8n4f2kz.html?file=${link}" frameborder="0"></iframe>
      </div>
      <div class="po-pup__content__info">
        <div>
          <h2 class="po-pup__content__info-title">${title}</h2>
          <p class="po-pup__content__info-text">Опубликовано: ${date}</p>
        </div><a class="btn btn-primary" href="#">Смотреть бесплатно</a>
      </div>
      <div class="po-pup__content__text">
        <div class="po-pup__content__text-description">${description}</div>
        <div class="po-pup__content__text-autor">
          <p> Автор: ${author}</p>
        </div>
        <div class="po-pup__content__text-social">
          <p>Поделиться отзывом:</p>
          <div class="po-pup__content__text-social-icons">
            <a class="vk" href="#"><img src="./images/svg/vk.svg" alt="vk"/></a><a class="telegram" href="#">
              <img src="./images/svg/telegram.svg" alt="telegram"/>
            </a>
            <a class="instagram" href="#">
              <img src="./images/svg/instagram.svg" alt="instagram"/>
            </a>
          </div>
        </div>
      </div>
  </div>`;
  if (position === "prepend") {
    $("#popup-slayder").prepend(item);
  } else {
    $("#popup-slayder").append(item);
  }
}

//*---------------------------------------------

// *filterFunction-------------------------
function sorting(data) {
  const listItem = Array.from($(".courses__item-js"));
  $(listItem).removeClass("sorted");
  let dataSpiker = null;
  let dataUniversity = null;
  let dataTrend = null;
  let dataInterest = "false";
  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      const element = data[key];
      if (element.length > 0) {
        switch (key) {
          case "spiker":
            dataSpiker = data[key];
            break;
          case "university":
            dataUniversity = data[key];
            break;
          case "trend":
            dataTrend = data[key];
            break;
          case "interest":
            dataInterest = data[key];
            break;
          default:
            break;
        }
      }
    }
  }
  let masSpiker;
  let masUniversity;
  let masTrend;
  let interest;
  let masDate;
  listItem.filter((element) => {
    let isHave = true;
    masSpiker = $(element).attr("data-value-spiker").split(",");
    masUniversity = $(element).attr("data-value-university").split(",");
    masTrend = $(element).attr("data-value-trend").split(",");
    interest = $(element).attr("data-interest");
    if (dataSpiker) {
      if (intersect(dataSpiker, masSpiker).length === 0) {
        isHave = false;
      }
    }
    if (dataUniversity) {
      if (intersect(dataUniversity, masUniversity).length === 0) {
        isHave = false;
      }
    }
    if (dataTrend) {
      if (intersect(dataTrend, masTrend).length === 0) {
        isHave = false;
      }
    }
    if (dataInterest !== "false") {
      if (dataInterest === interest) {
        isHave = false;
      }
    }
    if (isHave) {
      $(element).removeClass("bounceOut").addClass("sorted");
    }
  });

  $("#popup-slayder").trigger("destroy.owl.carousel");
  const listItems = Array.from($(".sorted"));
  // todo закончить сортировку по времени
  // listItems.forEach((el) => {
  //   masDate.push({
  //     dateNormalFormat: $(el).find(".date").text(),
  //     dateFormated: Date.parse($(el).find(".date").text().split(" ")[0]),
  //   });
  // });

  // masDate = masDate.sort((a, b) => {
  //   let f = a.dateFormated;
  //   let s = b.dateFormated;
  //   if (isNaN(f)) {
  //     f = 0;
  //   }
  //   if (isNaN(s)) {
  //     s = 0;
  //   }
  //   return s - f;
  // });

  // for (let i = 0; i < masDate.length; i++) {
  //   let idx;
  //   const date = masDate[i].dateNormalFormat;
  //   for (let b = 0; b < listItems.length; b++) {
  //     const element = listItems[b];
  //     if (date === $(element).find(".date").text()) {
  //       $(element)
  //         .find(".count")
  //         .text(i + 1);
  //       contentBody.append(element);
  //       listItems.splice(b, 1);
  //       break;
  //     }
  //   }
  // }
  showCourses(listItems);
}
function intersect(arr1, arr2) {
  return arr1.filter(function (n) {
    return arr2.indexOf(n) !== -1;
  });
}

// *==============-------------------------
