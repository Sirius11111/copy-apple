const gulp = require('gulp')
const serve = require('./gulp/tasks/serve')
const pug2html = require('./gulp/tasks/pug2html')
const pugHtmlParts = require('./gulp/tasks/pugHtmlParts')
const styles = require('./gulp/tasks/styles')
const script = require('./gulp/tasks/script')
const fonts = require('./gulp/tasks/fonts')
const libs = require('./gulp/tasks/libs')
const imageMinify = require('./gulp/tasks/imageMinify')
const imageWebp = require('./gulp/tasks/imageWebp')
const clean = require('./gulp/tasks/clean')
const copyDependencies = require('./gulp/tasks/copyDependencies')
const lighthouse = require('./gulp/tasks/lighthouse')
const svgSprite = require('./gulp/tasks/svgSprite')
const svgImages = require('./gulp/tasks/svgImages')
const deploy = require('./gulp/tasks/deploy')
const imageFolders = require('./gulp/tasks/imageFolders')

const dev = gulp.parallel(pug2html, styles, script, fonts, libs, imageMinify, imageWebp, svgSprite,svgImages, imageFolders)
const build = gulp.series(clean, copyDependencies, dev, imageMinify)

module.exports.start = gulp.series(build, serve)
module.exports.build = build
module.exports.deploy = deploy
module.exports.getParts = pugHtmlParts

module.exports.lighthouse = gulp.series(lighthouse)
