# GULP-guruIT. (-_-)-->

### Папки: 
    >"src/pages" — папка для наших страниц, где в корне хранятся непосредственно страницы
    >>"src/pages/common" — хранятся общие блоки для всех страниц
    >>"src/pages/includes" — хранятся модули страниц, где внутри еще одна папка, которая соответствует названию страницы
    >"src/pages/styles" - Папка Стилей
    >>"src/pages/common" - Использовать для одинаковых стилей
    >>"src/pages/mixins" - Использовать для Миксинов
    >>"src/pages/untils" - Хз для чего, пригодиться наверное)

    >"src/pages/js" - Папка Скриптов, ну тут все понятно) 

# Медиа запросы пишем прямо в теге.
### Команды gulp:
    "start": "gulp start",
    "build": "gulp build",
    "lighthouse": "gulp build && gulp lighthouse",
    "test": "editorconfig-cli && stylelint ./src/styles/**/*.scss --syntax scss && eslint ./src/js",
    "stylelint-fix": "stylelint ./src/styles/**/*.scss --fix --syntax scss",
    "eslint-fix": "eslint ./src/js/** --fix"