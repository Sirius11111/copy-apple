const gulp = require('gulp');
const plumber = require('gulp-plumber');
const pug = require('gulp-pug');
const pugLinter = require('gulp-pug-linter');
const htmlValidator = require('gulp-w3c-html-validator');
const gulpHtmlBemValidator = require('gulp-html-bem-validator');
const rename = require("gulp-rename");

module.exports = function pugHtmlParts(cb) {

    return gulp.src(['src/pages/**/*.pug'])
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(gulpHtmlBemValidator())
        // Разкомментировать при деплое
        // unComments in deploy
        /*.pipe(rename((path)=>{path.extname = ".php";})) */
        .pipe(gulp.dest('build/parts'))
}
