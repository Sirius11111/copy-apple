const gulp = require('gulp')
const imagemin = require('gulp-imagemin')

module.exports = function imageMinify() {
  return gulp.src('src/images/*.{gif,png,jpg,svg,webp}')
    // .pipe(imagemin())
    .pipe(gulp.dest('build/images'))
}
