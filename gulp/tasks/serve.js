const gulp = require('gulp')

const imageMinify = require('./imageMinify')
const svgSprite = require('./svgSprite')
const svgImages = require('./svgImages')
const styles = require('./styles')
const fonts = require('./fonts')
const pug2html = require('./pug2html')
const script = require('./script')
const copyDependencies = require('./copyDependencies')

const server = require('browser-sync').create()

module.exports = function serve(cb) {
    server.init({
        server: 'build',
        notify: false,
        open: true,
        cors: true,
        // index: "cooperation.html"
    })
    
    gulp.watch('src/styles/**/*.scss', gulp.series(styles, cb => gulp.src('build/css').pipe(server.stream()).on('end', cb)))
    gulp.watch('src/js/main.js', gulp.series(script)).on('change', server.reload)
    gulp.watch(['src/**/*.pug'], gulp.series(pug2html))
    gulp.watch('build/*.html').on('change', server.reload)

    return cb()
}
