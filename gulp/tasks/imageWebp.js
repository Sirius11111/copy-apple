const gulp = require('gulp')
const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');

module.exports = async function imageWebp() {
  const webp = await imagemin(
    ['src/images/*.{jpg,png}'], {
      destination: 'build/images/webp',
      plugins: [
        imageminWebp({
          quality: 75,
        })
      ]
    })
  console.log(webp);
  const _webp = await imagemin(
    ['src/images/*.{jpg,png}'], {
      destination: 'src/images/webp',
      plugins: [
        imageminWebp({
          quality: 75,
        })
      ]
    })
  console.log(_webp);
}
