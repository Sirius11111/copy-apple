const gulp = require('gulp')

module.exports = function fonts() {
  return gulp.src('src/images/svg/**/*')
    .pipe(gulp.dest('build/images/svg'))
}
