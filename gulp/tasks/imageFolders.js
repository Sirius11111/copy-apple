const gulp = require('gulp')

module.exports = function libs() {
  return gulp.src('src/images/favicon/**/*')
    .pipe(gulp.dest('build/images/favicon'))
}
