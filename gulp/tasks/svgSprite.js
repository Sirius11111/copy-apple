const gulp = require('gulp')
const svgstore = require('gulp-svgstore')
const svgmin = require('gulp-svgmin')
const cheerio = require('gulp-cheerio')
const rename = require('gulp-rename')
const replace = require('gulp-replace')

module.exports = function svgSprite() {
  return gulp.src('src/images/sprite/*.svg')
    .pipe(svgmin({js2svg: {pretty: true}}))
    .pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[style]').removeAttr('style');
			},
			parserOptions: { xmlMode: true }
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(rename('sprite.svg'))
    .pipe(gulp.dest('build/images/svg/i'))
}
